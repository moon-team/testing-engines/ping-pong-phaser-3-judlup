# Ping Pong Phaser 3

## Install and run

Run next commands in your terminal:

| Command | Description |
|---------|-------------|
| `npm install` | Install dependencies and launch browser with examples.|
| `npm run build:dev` | Builds a unminified copy of your game, with source maps, for debugging purposes. |
| `npm run build:prod` | Builds a minified copy of your game without source maps for production use. |
| `npm start` | Launch browser of choice and navigate to [http://localhost:8080/](http://localhost:8080/). <br> Press `Ctrl + C` in NodeJS terminal to kill **webpack-dev-server** process. |
=======

### Moon Team
[@MoonTeamDEV](https://twitter.com/moonteamdev)

[@Judlup](https://twitter.com/judlup)