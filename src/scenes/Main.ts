import { config } from '../config/preload';
import { throws } from 'assert';
import { Vector } from 'matter';
export class Main extends Phaser.Scene {
		
	public ballObj:any;
	public player1:any;
	public player2:any;
	public player1ScoreText:any;
	public player1Score:number = 0;
	public player2ScoreText:any;	
	public player2Score:number = 0;
	public up:any;
	public down:any;
	public w:any;
	public s:any;	
	public board:any;
	public pointer:any;

	constructor() {
		super("main");
	}

	
	preload(){
		this.load.image("ball",config.imagesPath+"ball1.png");
		this.load.image("paddle",config.imagesPath+"paddle.png");		
	}

	create() {
													
		this.player1					= this.paddle(this.sys.canvas.width * 0.05,this.sys.canvas.height / 3);		
		this.player1ScoreText	= this.text(this.sys.canvas.width / 1.5, this.sys.canvas.height / 20, this.player1Score.toString());
		this.player2					= this.paddle(this.sys.canvas.width * 0.90,this.sys.canvas.height / 3);
		this.player2ScoreText	= this.text(this.sys.canvas.width / 3.5, this.sys.canvas.height / 20, this.player2Score.toString());
		this.board						= this.text(this.sys.canvas.width / 3.2, this.sys.canvas.height / 1.1, "");		
		this.up 							= this.input.keyboard.addKey('UP');
		this.down 						= this.input.keyboard.addKey('DOWN');
		this.w 		 						= this.input.keyboard.addKey('W');
		this.s 		 						= this.input.keyboard.addKey('S');
		this.ballObj					= this.ball(this.sys.canvas.width / 2, this.sys.canvas.height / 2);				
		this.ballObj.body.isCircle = true;
		this.ballObj.setVelocity(400,800);
		this.pointer = this.input.activePointer;
	}

	paddle(x:number,y:number){	
		return this.physics.add.sprite(x,y,"paddle").setImmovable(true).setCollideWorldBounds(true);
	}

	ball(x:number,y:number){		
		return this.physics.add.sprite(x,y,"ball").setBounce(1,1).setCollideWorldBounds(true);
	}
	
	text(x:number, y:number, text:string){
		return this.add.text(x, y, text, { font: '25px Courier', fill: '#ffffff' }).setShadow(1, 1).setFont("25px Rajdhani");
	}

	update(){
		this.physics.world.collide(this.ballObj,[this.player1,this.player2]);		
		if (this.w.isDown){
			console.log("Player 1 UP");							
			this.player1.setVelocityY(-600);
		}	else if(this.s.isDown){
			console.log("Player 1 DOWN");
			this.player1.setVelocityY(600);
		}else{
			this.player1.setVelocityY(0);
		}

		if(this.up.isDown){
			console.log("Player 2 UP");
			this.player2.setVelocityY(-600);
		} else if(this.down.isDown){
			console.log("Player 2 DOWN");
			this.player2.setVelocityY(600);
		}else{
			this.player2.setVelocityY(0);
		}
		
		if(this.ballObj.x < this.player1.x - 4)
		{			
			this.goal();
			this.player2Score ++;
			this.player2ScoreText.setText(this.player2Score);
			console.log("Punto Player 2");
		}

		if(this.ballObj.x > this.player2.x + 4)
		{		
			this.goal();
			this.player1Score ++;
			this.player1ScoreText.setText(this.player1Score);
			console.log("Punto Player 1");
		}

		if(this.player1Score == 5){
			this.board.setText("Player 1 gana (reinicia dando click)");
			this.reset();
		}

		if(this.player2Score == 5){
			this.board.setText("Player 2 gana (reinicia dando click)");
			this.reset();
		}

		if(this.ballObj.body.velocity.x == 0 && this.ballObj.body.velocity.y == 0 && this.pointer.isDown){
			this.board.setText("");
			this.goal();
		}
		
	}

	goal(){
		this.player1.x = this.sys.canvas.width * 0.05;
		this.player1.y = this.sys.canvas.height / 3;
		this.player2.x = this.sys.canvas.width * 0.90;
		this.player2.y = this.sys.canvas.height / 3;
		this.ballObj.x = this.sys.canvas.width / 2;
		this.ballObj.y = this.sys.canvas.height / 2;		
		let rand = Math.floor(Math.random() * 2);
		let direccion;
		if(rand == 0){
			direccion = -1;
		}else{
			direccion = 1;
		}
		this.ballObj.setVelocity(400*direccion,800); //Phaser.Math.Between(-400, 400)
	}

	reset(){
		this.player1Score = 0;
		this.player1ScoreText.setText(this.player1Score);
		this.player2Score = 0;
		this.player2ScoreText.setText(this.player2Score);
		this.ballObj.setVelocity(0,0);
	}

}